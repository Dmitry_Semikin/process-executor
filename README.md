# README #

`process-executor` is minimalistic (just several classes) library,
which provides tools for convenient (in certain situations) execution
of external processes.

The idea for this library came, when I tried to use java instead of bash
to write some installation "script" (in java case it was not script, but
the idea is still the same). To conveniently write such a "script" one
needs to execute lots of external processes and control output (either
print it in real time into console or get it as list of strings, or both)
and return-values. So, the focus of this library is to provide those
facilities and to enable one to easyly write "scripts" in java.


### How do I get set up? ###

Just clone the repo and build it with Maven. Use `mvn install` to put it into local repo.
Then it can be used as a dependency in the projects. Or just include it as a subproject.


### Contribution guidelines ###

If you really like to do it, just contact me.


### Who do I talk to? ###

See pom file.