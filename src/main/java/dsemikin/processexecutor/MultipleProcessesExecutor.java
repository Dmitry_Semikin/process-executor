package dsemikin.processexecutor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MultipleProcessesExecutor implements AutoCloseable {

    private Path workingDirectory;
    private boolean isEchoCommandAndArgumentsToSystemOutput;
    private boolean isSuppressSystemOutput;
    private boolean isReturnProcessOutput;
    private boolean isAddProcessOutputToCumulativeProcessOutput;

    final private ByteArrayOutputStream cumulativeProcessOutputHandler;

    private FileOutputStream cumulativeLog;

    public MultipleProcessesExecutor() {

        workingDirectory = null;
        isEchoCommandAndArgumentsToSystemOutput = false;
        isReturnProcessOutput = false;
        isSuppressSystemOutput = false;
        isAddProcessOutputToCumulativeProcessOutput = false;
        cumulativeProcessOutputHandler = new ByteArrayOutputStream();
        cumulativeLog = null;

    }

    @Nullable
    public ProcessOutput execute(
            @Nonnull final String... commandAndArguments
    ) throws InterruptedException, ProcessErrorException, IOException {
        return buildProcessExecutor(commandAndArguments).execute();
    }

    public void setWorkingDirectory(
            @Nonnull final String workingDirectory
    ) {
        this.workingDirectory = Paths.get(workingDirectory);
    }

    public void setEchoCommandAndArgumentsToSystemOutput(final boolean isEchoCommandAndArgumentsToSystemOutput) {
        this.isEchoCommandAndArgumentsToSystemOutput = isEchoCommandAndArgumentsToSystemOutput;
    }

    public void setReturnProcessOutput(final boolean isReturnProcessOutput) {
        this.isReturnProcessOutput = isReturnProcessOutput;
    }

    public void setSuppressSystemOutput(final boolean suppressSystemOutput) {
        this.isSuppressSystemOutput = suppressSystemOutput;
    }

    public void setAddProcessOutputToCumulativeProcessOutput(final boolean isAddProcessOutputToCumulativeProcessOutput) {
        this.isAddProcessOutputToCumulativeProcessOutput = isAddProcessOutputToCumulativeProcessOutput;
    }

    public void writeCumulativeLog(@Nonnull final String logPath) throws IOException {
        initCumulativeLog(logPath, false);
    }

    public void appendCumulativeLog(@Nonnull final String logPath) throws IOException {
        initCumulativeLog(logPath, true);
    }

    public void closeCumulativeLog() throws IOException {
        if (cumulativeLog != null) {
            cumulativeLog.flush();
            cumulativeLog.close();
        }
        cumulativeLog = null;
    }

    @Nonnull
    public String getCumulativeProcessOutput() {
        return cumulativeProcessOutputHandler.toString();
    }

    @Nonnull
    private ProcessExecutor buildProcessExecutor(@Nonnull final String[] commandAndArguments) {

        ProcessExecutor executor = ProcessExecutor.command(commandAndArguments);

        if (workingDirectory != null) {
            executor.inWorkingDirectory(workingDirectory);
        }

        if (isEchoCommandAndArgumentsToSystemOutput) {
            executor.echoCommandAndArgumentsToSystemOutput();
        }

        if (isReturnProcessOutput) {
            executor.returnProcessOutput();
        }

        if (isSuppressSystemOutput) {
            executor.suppressSystemOutput();
        }

        if (isAddProcessOutputToCumulativeProcessOutput) {
            executor.withMergedOutputHandler(cumulativeProcessOutputHandler);
        }

        if (cumulativeLog != null) {
            executor.withMergedOutputHandler(cumulativeLog);
        }

        return executor;
    }

    @Override
    public void close() throws IOException {
        closeCumulativeLog();
    }

    private void initCumulativeLog(@Nonnull final String logPath, final boolean append) throws IOException {
        if (cumulativeLog != null) {
            closeCumulativeLog();
        }
        File cumulativeLogFile = new File(logPath);
        if (!cumulativeLogFile.exists()) {
            cumulativeLogFile.createNewFile();
        }
        cumulativeLog = new FileOutputStream(cumulativeLogFile, append);
    }
}
