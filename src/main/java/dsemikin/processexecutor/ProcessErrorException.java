package dsemikin.processexecutor;

public class ProcessErrorException extends Exception {

    private final int processReturnCode;

    public ProcessErrorException(int processReturnCode) {
        this.processReturnCode = processReturnCode;
    }

    public ProcessErrorException(int processReturnCode, String message) {
        super(message);
        this.processReturnCode = processReturnCode;
    }

    public ProcessErrorException(int processReturnCode, Throwable cause) {
        super(cause);
        this.processReturnCode = processReturnCode;
    }

    public ProcessErrorException(int processReturnCode, String message, Throwable cause) {
        super(message, cause);
        this.processReturnCode = processReturnCode;
    }

    public int getProcessReturnCode() {
        return processReturnCode;
    }
}
