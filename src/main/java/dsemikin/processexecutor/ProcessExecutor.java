package dsemikin.processexecutor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ideas:
 *  - execution is synchronous. I.e. current thread blocks until external process completes.
 *    Then we do not need methods to wait for the process to end or to check the status.
 *  - If return code is not 0, then exception is thrown, which has return code value.
 *    and stderr and stdout.
 *  - By default stdout and stderr are redirected to console. It may be suppressed by additional builder method.
 *
 * Examples
 *
 *     try {
 *         ProcessResult processResult = ProcessExecutor.command("ls", "-la").execute();
 *     } catch (PrecessExecutionErrorException e) {
 *         System.out.println("Process failed with return code %s", e.getReturnCode());
 *     }
 *
 *
 */
public class ProcessExecutor {

    private final List<String> commandAndArguments;
    private final ByteArrayOutputStream stdoutByteCollector;
    private final ByteArrayOutputStream stderrByteCollector;
    private final List<OutputStream> stdoutHandlers;
    private final List<OutputStream> stderrHandlers;
    private final List<Thread> processOutputHandlersThreads;

    private boolean isSuppressSystemOutput;
    private boolean isReturnProcessOutput;
    private boolean isEchoCommandAndArgumentsToSystemOutput;
    private Path workingDirectory;

    @Nonnull
    public static ProcessExecutor command(
            @Nonnull final String... commandAndArguments
    ) {
        return ProcessExecutor.command(Arrays.asList(commandAndArguments));
    }

    @Nonnull
    public static ProcessExecutor command(
            @Nonnull final List<String> commandAndArguments
    ) {
        return new ProcessExecutor(commandAndArguments);
    }

    private ProcessExecutor(
            @Nonnull final List<String> commandAndArguments
    ) {
        this.commandAndArguments = commandAndArguments;
        isReturnProcessOutput = false;
        isSuppressSystemOutput = false;
        isEchoCommandAndArgumentsToSystemOutput = false;
        stdoutByteCollector = new ByteArrayOutputStream();
        stderrByteCollector = new ByteArrayOutputStream();
        workingDirectory = Paths.get(".");
        stdoutHandlers = new ArrayList<>();
        stderrHandlers = new ArrayList<>();
        processOutputHandlersThreads = new ArrayList<>();
    }

    @Nonnull
    public ProcessExecutor suppressSystemOutput() {
        isSuppressSystemOutput = true;
        return this;
    }

    @Nonnull
    public ProcessExecutor returnProcessOutput() {
        isReturnProcessOutput = true;
        return this;
    }

    @Nonnull
    public ProcessExecutor echoCommandAndArgumentsToSystemOutput() {
        isEchoCommandAndArgumentsToSystemOutput = true;
        return this;
    }

    @Nonnull
    public ProcessExecutor inWorkingDirectory(
            @Nonnull final Path directoryPath
    ) {
        workingDirectory = directoryPath;
        return this;
    }

    @Nonnull
    public ProcessExecutor inWorkingDirectory(
            @Nonnull final String directoryPath
    ) {
        workingDirectory = Paths.get(directoryPath);
        return this;
    }

    @Nonnull
    public ProcessExecutor withStdoutHandler(
            @Nonnull final OutputStream stdoutHandler
    ) {
        stdoutHandlers.add(stdoutHandler);
        return this;
    }

    @Nonnull
    ProcessExecutor withStderrHandler(
            @Nonnull final OutputStream stderrHandler
    ) {
        stderrHandlers.add(stderrHandler);
        return this;
    }

    @Nonnull
    ProcessExecutor withMergedOutputHandler(
            @Nonnull final OutputStream outputHandler
    ) {
        stdoutHandlers.add(outputHandler);
        stderrHandlers.add(outputHandler);
        return this;
    }

    // TODO: implement appendEnvironmentVariable(String, String), prependEnvironmentVariable(String, String) and setEnvironmentVariable(String, String)

    @Nonnull
    public ProcessOutput execute() throws IOException, InterruptedException, ProcessErrorException {

        assert stdoutByteCollector != null;
        assert stderrByteCollector != null;
        assert commandAndArguments != null;
        assert workingDirectory != null;

        if (isEchoCommandAndArgumentsToSystemOutput) {
            // We disable warning below. The list may indeed have nulls, but then it is illegal state. Just let it crash.
            //noinspection ConstantConditions
            System.out.println(commandAndArguments.stream().map(str -> "\"" + str + "\"").collect(Collectors.joining("  ")));
        }

        final ProcessBuilder processBuilder = new ProcessBuilder();
        final Process process = processBuilder
                .command(commandAndArguments)
                .directory(workingDirectory.toFile())
                .start();

        List<InputStreamProcessor> processInputStreamProcessors = new ArrayList<>();
        List<InputStreamProcessor> processErrorStreamProcessors = new ArrayList<>();

        if (isReturnProcessOutput) {
            processInputStreamProcessors.add(stdoutByteCollector::write);
            processErrorStreamProcessors.add(stderrByteCollector::write);
        }

        if (!isSuppressSystemOutput) {
            processInputStreamProcessors.add(System.out::write);
            processErrorStreamProcessors.add(System.err::write);
        }

        for (final OutputStream stdoutHandler : stdoutHandlers) {
            processInputStreamProcessors.add(stdoutHandler::write);
        }

        for (final OutputStream stderrHandler : stderrHandlers) {
            processErrorStreamProcessors.add(stderrHandler::write);
        }

        if (!processInputStreamProcessors.isEmpty()) {
            InputStream processInputStream = process.getInputStream();
            if (processInputStream != null) {
                processOutputHandlersThreads.add(
                        startAsyncInputStreamProcessing(processInputStream, processInputStreamProcessors)
                );
            }
        }

        if (!processErrorStreamProcessors.isEmpty()) {
            InputStream processErrorStream = process.getErrorStream();
            if (processErrorStream != null) {
                processOutputHandlersThreads.add(
                        startAsyncInputStreamProcessing(processErrorStream, processErrorStreamProcessors)
                );
            }
        }

        int processReturnValue = process.waitFor();
        for (Thread processOutputHandlerThread : processOutputHandlersThreads) {
            processOutputHandlerThread.join();
        }

        if (processReturnValue != 0) {
            throw new ProcessErrorException(processReturnValue);
        }

        ProcessOutput processOutput;
        if (isReturnProcessOutput) {

            processOutput = new ProcessOutput(stdoutByteCollector.toString(), stderrByteCollector.toString());
        } else {
            processOutput = new ProcessOutput("", "");
        }
        return processOutput;
    }

    @Nonnull
    private Thread startAsyncInputStreamProcessing(
            @Nonnull final InputStream inputStream,
            @Nonnull final List<InputStreamProcessor> inputStreamProcessors
    ) {
        Thread thread = new Thread(() -> {
            try {
                for (int currentByteAsInt = inputStream.read(); currentByteAsInt != -1; currentByteAsInt = inputStream.read()) {
                    for (InputStreamProcessor inputStreamProcessor : inputStreamProcessors) {
                        inputStreamProcessor.processCurrentByte(currentByteAsInt);
                    }
                }
            } catch (IOException e) {
                System.err.println("Error reading process output.");
                System.err.println(e.toString());
            }
        });
        thread.setDaemon(true);
        // TODO: use `thread.setDefaultUncaughtExceptionHandler()` - think what to do with exception
        thread.start();
        return thread;
    }

    private interface InputStreamProcessor {
        void processCurrentByte(int byteAsInt) throws IOException;
    }

}
