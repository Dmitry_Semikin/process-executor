package dsemikin.processexecutor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ProcessOutput {

    private final String processOutput;
    private final String processError;

    ProcessOutput(
            @Nullable String processOutput,
            @Nullable String processError
    ) {
        this.processOutput = processOutput != null ? processOutput : "";
        this.processError = processError != null ? processError : "";
    }

    @Nonnull
    public String getProcessOutput() {
        return processOutput;
    }

    @Nonnull
    public String getProcessError() {
        return processError;
    }
}
