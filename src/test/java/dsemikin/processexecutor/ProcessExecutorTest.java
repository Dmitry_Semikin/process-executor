package dsemikin.processexecutor;

import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ProcessExecutorTest {
    @Before
    public void setUp() throws Exception {
        Assume.assumeTrue(isJjsAvailable());
    }

    @Test
    public void fakeTest() {
        System.out.println("Testing something");
        Assert.assertTrue(true);
    }

    @After
    public void tearDown() throws Exception {
    }

    private boolean isJjsAvailable() throws IOException, InterruptedException {
        final Runtime runtime = Runtime.getRuntime();
        final Process jjsProcess = runtime.exec("jjs -help");
        final int returnCode = jjsProcess.waitFor();
        return (returnCode == 0);
    }

}